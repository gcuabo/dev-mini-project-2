to Run: 
```
cd ./capsl
docker-compose up --build
```

access web container bash: 
```
docker exec -it capsl_web_1 bash
```

migrate: 
```
python manage.py migrate
```

Open localhost:8000 in browser


Create Superuser:

access the web container and run django createsuperuser: 
```
docker exec -it capsl_web_1 bash
python manage.py createsuperuser
```