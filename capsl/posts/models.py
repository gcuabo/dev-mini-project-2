from django.db import models

from app.mixins.modelMixin import ModelMixin, PrimaryKeyMixin

from threads.models import Thread
from boards.models import Board
from users.models import User


class Post(ModelMixin, PrimaryKeyMixin):
    thread = models.ForeignKey(Thread, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date_posted = models.DateTimeField(auto_now_add=True)
    post = models.TextField()


# TODO MOVE THIS
from django.db.models.signals import post_save
from django.dispatch import receiver


# Update thread last_user_post_id 
@receiver(post_save, sender=Post)
def after_save(sender, instance, created, **kwargs):
    if created: 
        thread = Thread.objects.get(id=instance.thread_id)
        thread.last_user_posted_id = instance.user_id
        thread.save()

        # Update the post count
        board = Board.objects.get(id=thread.board_id)
        prev_count = board.post_count
        board.post_count = board.post_count
        board.save()