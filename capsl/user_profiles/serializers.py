from rest_framework import serializers

from .models import UserProfile

class UserProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserProfile
        fields = (
            'about',
            'date_of_birth',
            'hometown',
            'present_location',
            'geo_location',
            'facebook',
            'website',
            'gender',
            'interests'
        )
