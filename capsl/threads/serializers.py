from rest_framework import serializers

from .models import Thread
from boards.serializers import BoardSerializer
from users.serializers import UserSerializer


class ThreadSerializer(serializers.ModelSerializer):
    board = BoardSerializer()
    last_user_posted = UserSerializer()

    class Meta:
        model = Thread
        fields = (
            'id', 
            'board', 
            'last_user_posted', 
            'sticky'
        )

