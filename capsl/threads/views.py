from django.shortcuts import redirect
from rest_framework import viewsets, permissions, status
from rest_framework.response import Response

from app.pagination import Pagination
from app.permissions import IsAdmin
from posts.models import Post
from posts.serializers import PostSerializer
from .models import Thread
from .serializers import ThreadSerializer


class ThreadViewSet(viewsets.ModelViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer
    permission_classes = [permissions.IsAuthenticated]

    # Get 
    def list(self, request, board_id=None):
        if board_id: 
            threads = Thread.objects.filter(board_id=board_id)
        else: 
            threads = Thread.objects.all()
            
        serializer = self.get_serializer(threads, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk, board_id=None):
        if board_id: 
            thread = Thread.objects.filter(id=pk, board_id=board_id).first()

            if not thread: 
                return Response(status=status.HTTP_404_NOT_FOUND)

            response = redirect('/api/threads/%s' % (pk))
            return response
        
        thread = Thread.objects.get(id=pk)
            
        serializer = self.get_serializer(thread)
        return Response(serializer.data)

    def create(self, request, board_id): 
        data = request.data

        thread = Thread.objects.create(
            board_id=board_id, 
            thread=data['thread'], 
        )

        serializer = self.get_serializer(thread)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ThreadStickyStatusViewSet(viewsets.ModelViewSet):
    queryset = Thread.objects.all()
    serializer_class = ThreadSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [IsAdmin, ]
        elif self.action == 'delete':
            self.permission_classes = [isAdmin, ]
        return super(self.__class__, self).get_permissions()

    # set to sticky on post
    def create(self, request, thread_id): 
        thread = Thread.objects.get(id=thread_id)
        thread.sticky = True

        serializer = self.get_serializer(thread)
        return Response(serializer.data)

    # set to sticky to false on post
    def delete(self, request, thread_id): 
        thread = Thread.objects.get(id=thread_id)
        thread.sticky = False

        serializer = self.get_serializer(thread)
        return Response(serializer.data)


class ThreadPostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    pagination_class = Pagination
    permission_classes = [permissions.IsAuthenticated]
    
    def get_queryset(self):
        qs = super().get_queryset() 
        return qs.filter(thread_id=self.kwargs['thread_id'])

    def list(self, request, thread_id):
        threads = self.filter_queryset(self.get_queryset())
            
        page = self.paginate_queryset(threads)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            result = self.get_paginated_response(serializer.data)
            data = result.data # pagination data
        else:
            serializer = self.get_serializer(threads, many=True)
            data = serializer.data
            
        return Response(data)

    def retrieve(self, request, pk, thread_id):

        post = super().get_queryset().filter(id=pk).first()

        if not post: 
            return Response(status=status.HTTP_404_NOT_FOUND)
            
        serializer = self.get_serializer(post)
        return Response(serializer.data)

    # create board
    def create(self, request, thread_id): 
        data = request.data

        post = Post.objects.create(
            post=data['post'], 
            thread_id=thread_id, 
            user_id=request.user.id
        )

        serializer = self.get_serializer(post)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
