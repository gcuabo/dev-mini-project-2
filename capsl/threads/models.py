from django.db import models

from app.mixins.modelMixin import ModelMixin, PrimaryKeyMixin

from boards.models import Board
from users.models import User


class Thread(ModelMixin, PrimaryKeyMixin):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    last_user_posted = models.ForeignKey(User, on_delete=models.DO_NOTHING, blank=True, null=True)
    sticky = models.BooleanField(default=False)
    thread = models.TextField()


# TODO MOVE THIS to signals.py
from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=Thread)
def after_save(sender, instance, created, **kwargs):
    if created: 
        # Update the Thread count
        # Not Ideal can be done in celery tasks
        board = Board.objects.get(id=instance.board_id)
        prev_count = board.thread_count
        board.thread_count = prev_count + 1
        board.save()