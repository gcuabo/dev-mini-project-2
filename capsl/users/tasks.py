import logging
 
from django.urls import reverse
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from app.celery import app
 
 
@app.task
def send_verification_email(user_id):
    UserModel = get_user_model()
    try:
        user = UserModel.objects.get(pk=user_id)
        # FIXME
        # sending email error
        # Send confirmation email
        send_mail(
            'Confirmation Email',
            'This is your confirmationemail.',
            'info@example.com',
            [user.email],
            fail_silently=True,
        )
    except UserModel.DoesNotExist:
        logging.warning("Tried to send verification email to non-existing user '%s'" % user_id)