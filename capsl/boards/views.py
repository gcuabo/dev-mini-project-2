from rest_framework import viewsets, permissions, status
from rest_framework.response import Response

from app.permissions import IsModerator
from .models import Board, Topic
from .serializers import TopicSerializer, BoardSerializer


class TopicViewSet(viewsets.ModelViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [IsModerator, ]
        elif self.action == 'update':
            self.permission_classes = [IsModerator, ]
        return super(self.__class__, self).get_permissions()

    # create post
    def create(self, request): 
        data = request.data

        topic = Topic.objects.create(
            name=data['topic']
        )
        serializer = self.get_serializer(topic)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class BoardViewSet(viewsets.ModelViewSet):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_permissions(self):
        if self.action == 'create':
            self.permission_classes = [IsModerator, ]
        elif self.action == 'update':
            self.permission_classes = [IsModerator, ]
        return super(self.__class__, self).get_permissions()

    # create post
    def create(self, request):
        data = request.data

        board = Board.objects.create(
            name=data['topic'], 
            description=data['description'],
        )
        serializer = self.get_serializer(board)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
