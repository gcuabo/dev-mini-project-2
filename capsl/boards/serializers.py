from rest_framework import serializers

from .models import Board, Topic

class TopicSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Topic
        fields = (
            'id', 
            'name'
        )


class BoardSerializer(serializers.ModelSerializer):
    topic = TopicSerializer()

    class Meta:
        model = Board
        fields = (
            'id', 
            'name', 
            'description', 
            'thread_count', 
            'post_count',
            'order',
            'topic'
        )

